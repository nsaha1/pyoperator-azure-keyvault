import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="pyoperator-azure-keyvault-nilaysaha", # Replace with your own username
    version="0.0.1",
    author="Nilay Saha",
    author_email="nsaha@ambidexter.gmbh",
    description="Kubernetes operator for azure keyvault based on kopf",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://github.com/nilaysaha/azure-pyoperator-keyvault",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3.8",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.8',
)
