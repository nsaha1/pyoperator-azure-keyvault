# Package for kopf operator for azure keyvault.

This package helps in operations related to azure keyvault from kubernetes. The deployment is based on https://kopf.readthedocs.io/en/latest/
Basic goal of this operator is to pull in secrets from the azure key vault and set them as environment variables in the kubernetes.
The specification of the source (keyvault) and the target(the kubernetes) is set as part of the obj.yaml definition. The operator
itself is run using kopf and is deployed as a container inside the kubernetes (using deployment.yaml). Only one instance of the operator
is enough for the entire kubernetes as it listens to events from the kubernetes API and takes actions whenever it receives events of
creation, deletion, (edit: TODO) of the crd objects for this operator.

(Readme will be updated....WIP)