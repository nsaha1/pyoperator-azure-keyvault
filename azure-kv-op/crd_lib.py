import asyncio
import logging
from kubernetes import client, config, watch

logger = logging.getLogger('k8s_events')
logger.setLevel(logging.DEBUG)

config.load_kube_config()

v1 = client.CoreV1Api()
v1ext = client.ExtensionsV1beta1Api()

def get_secrets():
    label_selector = 'sname=azure-secrets'
    secrets = v1.list_secret_for_all_namespaces(label_selector=label_selector, resource_version=0)
    print(secrets)

async def pods():
    w = watch.Watch()
    for event in w.stream(v1.list_pod_for_all_namespaces):
        logger.info("Event: %s %s %s" % (event['type'], event['object'].kind, event['object'].metadata.name))
        await asyncio.sleep(0) 
    
async def deployments():
    w = watch.Watch()
    for event in w.stream(v1ext.list_deployment_for_all_namespaces):
        logger.info("Event: %s %s %s" % (event['type'], event['object'].kind, event['object'].metadata.name))
        await asyncio.sleep(0)


async def secrets():
    w = watch.Watch()
    last_seen_version = 0;
    label_selector = 'sname=azure-secrets'
    for event in w.stream(v1.list_secret_for_all_namespaces, label_selector=label_selector):
        print("Event: %s %s %s in namespace %s" % (event['type'], event['object'].kind, event['object'].metadata.name, event['object'].metadata.namespace))
        await asyncio.sleep(0)

if __name__ == '__main__':
    ioloop = asyncio.get_event_loop()    
    ioloop.create_task(secrets())
    ioloop.run_forever()
