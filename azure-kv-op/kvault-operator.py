import os
import kopf
import fsecrets
import k8sops
import json


def process_secrets(secretname, namespace, detail):
        print(f"input for process_secrets is: secretname:{secretname} namespace:{namespace} and detail:{detail}\n")
        sdetail = detail['vaultOp']
        vault = sdetail['name']
        key   = sdetail['object']['name']
        version = sdetail['object']['version']
        secret = fsecrets.get_secret(key, version, vault)
        
        print(f"For the vault:{vault} key:{key} fetching secret\n")
        print(f"fetched secret is:{secret}")
        
        outputkey  = sdetail['output']['dataKey']
        print(f"create ns secret: {secretname} in {namespace} ")        
        return (outputkey, secret)


def _create_secret(body):
        metadata    = body["metadata"]
        cr_obj_name = metadata["name"]
        namespace   = metadata['namespace']

        print(f"name:{cr_obj_name}\n")
        print(f"namespace:{namespace}\n")

        spec    = body["spec"]
        details = spec['details']
        secretname = spec['secretname']
        processed_secrets =  [process_secrets(secretname, namespace, x) for x in details]
        sdict = {}

        print(f"details:{details}\n")
        
        for (k,v) in processed_secrets: 
                sdict[k] = v
                
        secret = k8sops.create_ns_secret(secretname, namespace, sdict, [body])                
        return secret

        
def _delete_secret(secretname, namespace):
        try:
          print(f"Now deleting secret:{secretname} in namespace:{namespace}")
          k8sops.delete_ns_secret(secretname,namespace)
        except:
          kopf.exception(body, reason="SomeReason", message="Some exception: Could not delete secret:%s in namespace:%s"%(secretname, namespace))



def _update_secrets(body, namespace):
    """
    TODO: sub-optimal as of now: duplicate data in body and old_spec. Will have to be worked upon. 
    """        
    try:
        secretname = body["spec"]["secretname"]

        #first delete the secret of the old spec
        _delete_secret(secretname, namespace)
        
        #And then create the secrets of the new spec
        _create_secret(body)
    except:
        kopf.exception(body, reason="SomeReason", message="Some exception: Could not update secret:%s in namespace:%s"%(secretname, namespace))



@kopf.on.resume('yunar.de', 'v1', 'azurekeyvaults')
@kopf.on.create('yunar.de', 'v1', 'azurekeyvaults')
def create_fn(body, **kwargs):        
    try:
        secret = _create_secret(body)
        return {secret_created: secret}
    except:
        secretname = body['spec']['secretname']
        namespace  = body['metadata']['namespace']
        kopf.exception(body, reason="SomeReason", message="Some exception: Could not create secret:%s in namespace:%s"%(secretname, namespace))

        
@kopf.on.update('yunar.de', 'v1', 'azurekeyvaults')
def update_fn(body, spec, old, new, diff, namespace, meta, **_):
    (name, uid) = (meta['name'], meta['uid'])    
    print(f"Updated the following crd: NAME:{name} and UID:{uid}")
    print(f"The value of BODY IN UPDATE IS:{body}")
    print(f"Update FN: Detected update of crd. Body is {spec}\n old:{old}\n new:{new}\n meta:{meta}\n and diff:{diff}\n ")    
    try:
        body['spec'] = new['spec']   #transform the spec to the new one. Metadata remains same.
        _update_secrets(body, namespace)
    except:
        kopf.exception(body, reason="Not able to update the secrets corresponding to crd changes {diff}")

@kopf.on.cleanup()
def cleanup_fn(logger, **kwargs):
    pass


import threading, asyncio, crd_lib

def kopf_thread():
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    loop.run_until_complete(kopf.operator())

def monitor_secrets():
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    loop.run_until_complete(crd_lib.secrets())
    
def main():
    x = threading.Thread(target=kopf_thread)
    y = threading.Thread(target=monitor_secrets)

    x.start()    
    y.start()

    x.join()
    y.join()

if __name__== "__main__":
    main()
