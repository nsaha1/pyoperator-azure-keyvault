#!/usr/bin/env python
import os, sys
from kubernetes import client, config, watch
from kubernetes.client.rest import ApiException
import base64, pprint
import asyncio
import dfunc
import json

SECRET_TYPE="yunar.de/azure-secrets"
SECRET_LABEL="azure-secrets"
SECRET_LABEL_SELECTOR="stype:azure_secrets"
KOPF_FINALIZER="kopf.zalando.org/KopfFinalizerMarker"

def _encode(input):
    import subprocess
    print(f'Encoding :{input}')
    return os.popen("echo -n '%s' | base64"%(input)).read().split("\n")[0]

def _authcluster():
    if (os.getenv('KUBERNETES_SERVICE_HOST') != None):
        config.load_incluster_config()     #This is when the code is run inside a kubernetes cluster
    else:
        #This is for debug purpose when run from localhost
        os.path.join(os.environ["HOME"], '.kube/config')
        config.load_kube_config()

        
def delete_ns_secret(name, namespace):
    """
    This function deletes secret given name and namespace
    """
    _authcluster()
    try:
        api_instance = client.CoreV1Api()
        body = client.V1DeleteOptions()
        print(f"Deleting {name} in {namespace} in the delete_ns_secret")
        api_instance.delete_namespaced_secret(name, namespace)
    except ApiException as e:
        print("Exception when calling CoreV1Api->delete_namespaced_secret: %s\n" % e)

def _get_owner_references(owners=[]):
    ownerReferences = []
    for owner in owners:
        print(owner)
        api_version = owner["apiVersion"]
        kind = owner["kind"]
        name = owner["metadata"]["name"]
        uid  = owner["metadata"]["uid"]
        owner_ref = client.V1OwnerReference(api_version=api_version, kind=kind, name=name, uid=uid, block_owner_deletion=True)
        print(owner_ref)
        ownerReferences.append(owner_ref)
        print("inside _get_owner_references. Returning:{ownerReferences}")

    print(ownerReferences)
    return ownerReferences

def _get_req_body(name, namespace, kvdict={}, owners=[]):
    """
    Construct the body of the request.
    """
    print(f"Inside the _get_req_body")
    data = {}
    for k,v in kvdict.items():
        data[k] =  _encode(v)
    api_version = 'v1'
    kind = 'Secret'
    
    try:
        _authcluster()
        owner_references = _get_owner_references(owners)
        finalizers = []
        print(f"Owner references:{owner_references}")
        metadata = client.V1ObjectMeta(name= name, namespace= namespace, labels={"sname":SECRET_LABEL}, owner_references=owner_references, finalizers=finalizers)
        return client.V1Secret(api_version, data , kind, metadata, type=SECRET_TYPE)
    except ApiException as e:
        print("Exception when creating body of request: %s\n" % e)        

                
def create_ns_secret(name, namespace, kvdict = {}, owners=[]):
    """
    This function creates secrets in kvdict for the namespace of kubernetes. 
    """
    print(f"CREATING NAME:{name} WITHIN NS:{namespace} FOR ITEMS:{kvdict}")

    try:
        delete_ns_secret(name, namespace)
    except ApiException as e:
        print("Exception when calling CoreV1Api->delete_namespaced_secret: %s\n" % e)
        return ''
    finally:
        _authcluster()
        api_instance = client.CoreV1Api()
        body = _get_req_body(name, namespace, kvdict, owners)
        print(f"FINALLY CREATING SECRET IN NAMESPACE:${namespace} using body:{body}")        
        api_response = api_instance.create_namespaced_secret(namespace, body)
        print(api_response)
        return api_response


    
if __name__ == '__main__':
    create_ns_secret("tsecret", "nsaha-testspace", {'key1':'val1', 'key2':'val2'}) 
    #delete_ns_secret("tsecret", "nsaha-testspace")  
    #_monitor_secrets()
