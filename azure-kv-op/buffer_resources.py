import asyncio

GLOBAL_BUFFER={}


def _get_hash(secret_val, namespace):
    s= "%s:%s"%(secret_val, namespace)
    s1= s.encode('utf-8')
    return hashlib.sha1(s1).hexdigest()


def add_pair_cs(cr_val, secret_val, namespace):
    hval = _get_hash(secret_val, namespace)
    if hval in GLOBAL_BUFFER.keys():
        GLOBAL_BUFFER[hval] = cr_val
    
def delete_pair_cs(secret_val, namespace):
    hval = _get_hash(secret_val, namespace)
    if hval in GLOBAL_BUFFER.keys():
        del GLOBAL_BUFFER[hval]

        
def fetch_cr_from_secret(secret_val, namespace):
    hval = _get_hash(secret_val, namespace)
    #if hval in GLOBAL_BUFFER.keys():
        
