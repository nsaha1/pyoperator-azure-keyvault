#!/usr/bin/env python
import datetime
import os
import sys
from dotenv import load_dotenv
load_dotenv()
from azure.keyvault.secrets import SecretClient
from azure.identity import ClientSecretCredential
from azure.core.exceptions import HttpResponseError

def get_secret( name, version, vaultName=os.environ['KV_NAME']):
    """
    This gets the secret from keyvault with keyname = name and version = version
    """
    KVUri = "https://" + vaultName + ".vault.azure.net";
    credential = ClientSecretCredential(os.environ["KV_TENANT"], os.environ["KV_CLIENT"], os.environ["KV_SECRET"])
    client = SecretClient(KVUri, credential)
    try:
        print('trying to get secret value')
        secret_bundle = client.get_secret(name)
        return secret_bundle.value
    except:
        print(sys.exc_info())
        return ''


def sync_secrets():
    """
    Will at interval run a check on a particular secret and sync it back to the kubernetes env.
    """
    
if __name__ == '__main__':
    print(get_secret("aaaa-mdotest123", "467c6d9609344d3899dc71743e6ac9a9"))
